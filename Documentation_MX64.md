Infos gotten from https://emanual.robotis.com/docs/en/dxl/mx/mx-64/

3 different control modes : 
* Position
* Velocity 
* Torque

# Activation of modes

## Angles limits / modes

Different modes of servomotors will be set by the parameters Angles Limits of CW (= ClockWise) (6) and CCW (CounterClockWise) (8) : 

* Wheel Mode : 
    - 
    - Condition : CW = 0 AND CCW = 0

* Multi-turn Mode : 
    -
    - Condition : CW = 4095 AND CCW = 4095
* Joint Mode : 
    - Controls position from 0 - 4095 (or 0 - 360 °) (1 unit --> 0.088°)
        * Cannot be superior to set CW / CCW limits 
        * CW : from 2048 - 4095 
        * CCW : from 0 - 2048
    - Condition : CW, CCW have different values from other cases

## Position

* Torque enable (24) = 1
* Angles limits can be set in CW (= ClockWise) (6) and CCW (CounterClockWise) (8)
    - Clockwise : 
* Set goal position (30) : 
    * If multi-turn mode enabled : 
        - 
    * Else : 
        - 