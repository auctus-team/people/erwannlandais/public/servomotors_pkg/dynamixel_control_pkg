#!/usr/bin/env python


## Documentation : https://emanual.robotis.com/docs/en/dxl/mx/mx-64/

import os
import rospy
from dynamixel_sdk import *
from dynamixel_sdk_examples.srv import *
from dynamixel_sdk_examples.msg import *

import time

import numpy as np

D_ADDR_TORQUE_ENABLE_MODEL = {
    "MX-64" : 24
}

D_ADDR_GOAL_POSITION = {
    "MX-64" : 30
}

D_ADDR_PRESENT_POSITION = {
    "MX-64" : 36
}

D_ADDR_BAUD_RATE = {
    "MX-64" : 4
}

D_ADDR_CW_ANGLE_LIMIT = {
    "MX-64" : 6

}
    
D_ADDR_CCW_ANGLE_LIMIT = {
    "MX-64" : 8
}

D_BAUDRATE_ADDRESS = {
    2e6 : 0,
    1e6 : 1,
    500e3 : 3,
    400e3 : 4,
    250e3 : 7,
    200e3 : 9,
    115200 : 16,
    57600 : 34,
    19200 : 103,
    9600 : 207
}

D_ADDR_ID = {
    "MX-64":3
}


L_ALL_SERVOS = ["MX-64"]

PROTOCOL_VERSION = 1.0

TORQUE_ENABLE               = 1                 # Value for enabling the torque
TORQUE_DISABLE              = 0                 # Value for disabling the torque
DXL_MINIMUM_POSITION_VALUE  = 0               # Dynamixel will rotate between this value
DXL_MAXIMUM_POSITION_VALUE  = 1000            # and this value (note that the Dynamixel would not move when the position value is out of movable range. Check e-manual about the range of the Dynamixel you use.)
DXL_MOVING_STATUS_THRESHOLD = 20                # Dynamixel moving status threshold

if os.name == 'nt':
    import msvcrt
    def getch():
        return msvcrt.getch().decode()
else:
    import sys, tty, termios
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    def getch():
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
    
class dynamixelBasis():
    """!
    
    Regroups all possible commands for one servo, put as standalone or in daisy configuration

    """

    # def generic_call(self):
    #     """!
        
    #     Method should be put in all other stuffs
        
    #     """
    #     raise NotImplementedError()

    def __init__(self, deviceName, servoType, dxl_id, workingBaud = -1):
        
        self.dxl_id = dxl_id
        self.deviceName = deviceName
        self.specialCmdsList = []

        if servoType in L_ALL_SERVOS:
            self.servoType = servoType
        else:
            print("ERROR : SERVOMOTEUR %s NOT SUPPORTED BY LIBRARY"%servoType)
            print("All available servos : ", L_ALL_SERVOS)
            quit()
        L_ADD_BAUDS = list(D_BAUDRATE_ADDRESS.keys())
        if workingBaud in L_ADD_BAUDS or workingBaud == -1:
            self.baudRate = workingBaud

        ## input of commands
        self.verbose = False
        self.dataIn = None
        self.byte = 0
        self.D_used = {}
        self.strTypeCmd = ""
        self.specialCmd = ""
        ## possible modes : 
        ## > 0 : write
        ## < 0 : read
        ## |1| : tx
        ## |2| : rx
        ## |3| : txRx
        self.txRxMode = 0

        ## output of commands
        self.success = False
        self.dxl_comm_result = None
        self.dxl_error = None
        self.dataOut = None


    def try_all_baudrate_dxl_id(self, tested_dxl, L_baud):
        """!
        
        tested_dxl : list of all dxl_id tested

        """
        allBauds = list( D_BAUDRATE_ADDRESS.keys())
        success = False
        self.verbose = True
        j = 0
        workingDxl = -1
        while not success and j < len(tested_dxl):
            self.dxl_id = tested_dxl[j]
            print("========= tested dxl : ", self.dxl_id)
            k = 0
            workingBaud = -1
            while not success and k < len(allBauds):
                if allBauds[k] in L_baud or len(L_baud) == 0:
                    success = True
                    baud = allBauds[k]
                    print("tried baud : ", baud)

                    # Open port

                    self.specialCmd = "OPENPORT"
                    self.generic_call()
                    success = self.success

                    if not success:
                        print("No open port")


                    if success:   
                    # Set port baudrate
                        self.specialCmd = "SETBAUD"
                        self.strTypeCmd = "Baud test (port enable)"
                        self.dataIn = baud
                        self.generic_call()
                        success = self.success

                    if success:
                        ## try enable torque command
                        self.byte = 1
                        self.D_used = D_ADDR_TORQUE_ENABLE_MODEL
                        self.dataIn = TORQUE_ENABLE
                        self.txRxMode = 3
                        self.strTypeCmd = "Baud Test (torque enable) "

                        self.generic_call()

                        success = self.success
                    
                    if success : 
                        workingBaud = baud

                k+=1
            
            if not success and k >= len(allBauds):
                j+=1

        if success:
            print("working baud and dxl : ", workingBaud, self.dxl_id)

        return(workingBaud)

    def try_factory_reset(self):

        if (self.baudRate!= -1):
            self.strTypeCmd = "Factory reset"
            self.specialCmd = "RESET"
            self.generic_call()


    def get_present_pos(self):
        if (self.baudRate!= -1):
            self.strTypeCmd = "Get position"
            self.txRxMode = -3
            self.D_used = D_ADDR_PRESENT_POSITION
            self.byte = 4
            self.generic_call()
    
    def set_joint_mode(self, packetHandler, portHandler, minValAng = 0.0, maxValAng = 360.0):
        """!
        
        Joint mode : control on position, between CW min value and CCW max value

        Controls on 0 - 360° maximum 
        
        """

        if (minValAng >= maxValAng):
            minValAng = np.max(0.0, maxValAng - self.int_to_ang(1))

        ## set CW value

    def set_CW_CCW_value(self,  packetHandler, portHandler, value, isCW):

        D_ADDR_USED = {}

        if (isCW):
            D_ADDR_USED = D_ADDR_CW_ANGLE_LIMIT
        
        else:
            D_ADDR_USED = D_ADDR_CCW_ANGLE_LIMIT

    def ang_to_int(self, angVal):
        """!
        
        Joint mode : 1 unit ==> 0.088°

        """
        intVal = int(angVal/0.088)
        return(intVal)


    def int_to_ang(self, intVal):
        """!
        
        Joint mode : 1 unit ==> 0.088°

        """
        angVal = intVal*0.088
        return(angVal)
    
    def set_id_dxl(self, data):
        if (self.baudRate!= -1):
            self.dataIn = data
            self.byte = 1
            self.D_used = D_ADDR_ID
            self.strTypeCmd = "ID SET"
            self.txRxMode = 3
            self.generic_call()

    def set_baudrate(self,data):
        if (self.baudRate!= -1):
            self.dataIn = data
            self.byte = 1
            self.D_used = D_ADDR_BAUD_RATE
            self.strTypeCmd = "Baudrate SET"
            self.txRxMode = 3
            self.generic_call()        


    
    # def set_pos(self,data):
    #     success = False
    #     dxl_comm_result, dxl_error = packetHandler.write4ByteTxRx(portHandler, self.dxl_id, D_ADDR_GOAL_POSITION[self.servoType], position)
    #     if dxl_comm_result != COMM_SUCCESS:
    #         print("SET POSITION FAILURE : %s" % packetHandler.getTxRxResult(dxl_comm_result))

    #     elif dxl_error != 0:
    #         print("SET POSITION ERROR : %s" % packetHandler.getRxPacketError(dxl_error))

    #     else:
    #         print("Position set")
    #         success = True
    #     return(success)

class MultipleServo():
    """!
    
    Manage for servomotors put in daisy chain

    To do so, use GroupSynchRead/Write
    
    """
    def __init__(self):
        a = 1

class OneServoStandalone(dynamixelBasis):
    """!
    
    Regroup commands for one standalone servo

    """

    def generic_call(self):
        success = False        
        dxl_comm_result = None
        dxl_error = None
        dataOut = None


        verbose = self.verbose
        data = self.dataIn
        byte = self.byte
        D_used = self.D_used
        strTypeCmd = self.strTypeCmd

        specialCmd = self.specialCmd

        txRxMode = self.txRxMode

        if self.specialCmd in self.specialCmdsList:

            if specialCmd == "RESET":
                ## Special command 1 : reset
                strTypeCmd = "FACTORY RESET"
                dxl_comm_result, dxl_error = self.packetHandler.factoryReset(self.portHandler, self.dxl_id)
                if dxl_comm_result == COMM_SUCCESS:
                    dxl_error = 0
                
            
            elif specialCmd == "OPENPORT":
                try:
                    strTypeCmd = "open port"
                    self.portHandler.openPort()
                    dxl_comm_result = COMM_SUCCESS
                    dxl_error = 0

                except:
                    if (verbose):
                        print("Failed to open the port")
                        verbose = False

            elif specialCmd == "SETBAUD":
                try:
                    self.portHandler.setBaudRate(data)
                    dxl_comm_result = COMM_SUCCESS
                    dxl_error = 0
                except:
                    if verbose:
                        print("Failed to change port baudrate")
                        verbose = False


            self.specialCmd = ""

        else:
            ## Set command according to byte value
            if (byte == 1):
                if txRxMode == 3:
                    ## tx and rx mode
                    dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, self.dxl_id, D_used[self.servoType], data)
            
            elif (byte == 2):
                if txRxMode == 3:
                    ## tx and rx mode
                    dxl_comm_result, dxl_error = self.packetHandler.write2ByteTxRx(self.portHandler, self.dxl_id, D_used[self.servoType], data)
            
            elif (byte == 4):
                if txRxMode == 3:
                    ## tx and rx mode / write
                    dxl_comm_result, dxl_error = self.packetHandler.write4ByteTxRx(self.portHandler, self.dxl_id, D_used[self.servoType], data)
            
                elif txRxMode == -3:
                    ## tx and rx mode / read
                    dataOut, dxl_comm_result, dxl_error = self.packetHandler.read4ByteTxRx(self.portHandler, self.dxl_id, D_used[self.servoType])
            

        ## Manage command output

        if dxl_comm_result != COMM_SUCCESS:
            if verbose:
                print("FAILURE FOR %s : %s" %(strTypeCmd,self.packetHandler.getTxRxResult(dxl_comm_result)) )

        elif dxl_error != 0:
            if verbose:
                print("ERROR FOR %s : %s" %(strTypeCmd, self.packetHandler.getRxPacketError(dxl_error)))

        else:
            if verbose:
                print("Success of %s"%strTypeCmd)
            success = True

        self.success = success
        self.dxl_comm_result = dxl_comm_result
        self.dxl_error = dxl_error
        self.dataOut = dataOut



    def __init__(self, deviceName, servoType, dxl_id, tryReset = False):
        
        super().__init__(deviceName,servoType,dxl_id)

        self.portHandler =PortHandler(self.deviceName)
        self.packetHandler = PacketHandler(PROTOCOL_VERSION)

        self.specialCmdsList = ["RESET","OPENPORT","SETBAUD"]
        ## best value after empirical tests
        self.durSleep = 0.1
        self.verbose = False


        self.try_all_baudrate_dxl_id(list(range(1,10)))

        print("baudrate : ", self.baudRate)
        print("dxl_id : ", self.dxl_id)

        if (tryReset):
            self.try_factory_reset()

            self.try_all_baudrate_dxl_id(list(range(1,10)))


    def get_present_pos(self):
        return super().get_present_pos()

    def try_all_baudrate_dxl_id(self, L_id, L_baud = []):
        self.baudRate = super().try_all_baudrate_dxl_id( L_id, L_baud)
    

    def try_factory_reset(self):
        super().try_factory_reset()

        if (self.success):
            time.sleep(self.durSleep)

            ## reset all values
            self.portHandler.ser.reset_input_buffer()
            self.portHandler.clearPort()
            self.portHandler.closePort()

            self.portHandler =PortHandler(self.deviceName)
            self.packetHandler = PacketHandler(PROTOCOL_VERSION)

            self.try_all_baudrate_dxl_id(list(range(1,10)))

            if (self.baudRate == -1):
                print("FACTORY RESET FAILED.")



    def set_id_dxl(self, data):
        super().set_id_dxl(data)

        if (self.success):
            self.dxl_id = data
            ## need to sleep a little bit for changes to be active
            time.sleep(self.durSleep)


    def set_baudrate(self,data):
        desired_baud = int(data)
        dta = D_BAUDRATE_ADDRESS[desired_baud]
        super().set_baudrate( dta )

        if (self.success):
            time.sleep(self.durSleep)
            ## check if it really worked
            self.baudRate = self.try_all_baudrate_dxl_id([self.dxl_id],[data])



if __name__ == "__main__":
    ## Controls to add : 
    ## - Factory reset --> ok
    ## - BaudRate setting --> ok
    ## - ID setting --> ok
    ## Add automatic detector for ID setting AND baudRate setting (same format as try_all)
    ## wanted dxl_id
    DXL_ID                      = 1                 # Dynamixel ID : 1
    DEVICENAME                  = '/dev/ttyUSB0'    # Check which port is being used on your controller
    SERVOTYPE = "MX-64"
    ## For reboot
    # OneServoStandalone(DEVICENAME,SERVOTYPE,DXL_ID, True)

    oSS= OneServoStandalone(DEVICENAME,SERVOTYPE,DXL_ID, False)


    ## test procedure

    s = input("Do you want to change? Y/N")

    if s == "Y" or s == "y":
        desired_id = 1
        desired_baudrate = 2e6

        state = 0

        complete = False
        curPos = -1

        while oSS.success and not complete:
            if state == 0:
                oSS.get_present_pos()

            if state ==1:
                print("pos : ", oSS.int_to_ang(oSS.dataOut) )
                # if (oSS.dxl_id == 1):
                #     dta = 2
                oSS.set_id_dxl(desired_id)

            if state == 2 or state == 4:
                oSS.get_present_pos()
            
            if state== 3:
                print("pos after id reset : ", oSS.int_to_ang(oSS.dataOut)   )
                oSS.set_baudrate(desired_baudrate)
            
            if state == 5:
                print("pos after baudrate set : ", oSS.int_to_ang(oSS.dataOut)   )
                complete = True
                
            if oSS.success:
                state+=1
        if oSS.success:    
            print("RESET SUCCESSFUL")