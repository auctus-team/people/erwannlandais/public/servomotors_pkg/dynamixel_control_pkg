# Description

Package for setting up static parameters on servomotors (ID, baudrate, ...). Written in Python, with dynamixel_api.py. This code is based on dynamixelSDK package (https://github.com/ROBOTIS-GIT/DynamixelSDK).

For using the servomotors for movement (ex : displacement of robot), please check dynamixel_interface package. 

# Instructions for dynamixel servomotors

## For every version

For this to work, you need to put user in groups tty and dialout (as stated in https://github.com/esp8266/source-code-examples/issues/26) : 

~~~
sudo usermod -a -G tty $USER
sudo usermod -a -G dialout $USER
~~~

Then, deconnect / reconnect for change to be active.

Furthermore, you may also need to add access to the concerned tty port.
To get the concerned port, check with dmesg. Here, the concerned port is ttyUSB0.

You will need to do the following operations each time you connect again the USB device : 

~~~
sudo chown $USER ttyUSB0
sudo setserial /dev/ttyUSB0 low_latency
~~~

To check the latency, do : 
~~~
cat /sys/bus/usb-serial/devices/ttyUSB0/latency_timer
~~~

